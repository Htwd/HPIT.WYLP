﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(HPIT.MVC.Startup))]
namespace HPIT.MVC
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
