﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace HPIT.MVC
{

    //路由配置
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");


            //Atricle/2017/08/07/100

            routes.MapRoute(
               name: "Atricle",
               url: "Atricle/{year}/{month}/{day}/{id}.html",
               defaults: new { controller = "Atricle", action = "Index", id = UrlParameter.Optional }
               //constraints: new { year = @"^\d{4}$" }
           );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
