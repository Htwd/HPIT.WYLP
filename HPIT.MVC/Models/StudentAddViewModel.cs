﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HPIT.MVC.Models
{
    /// <summary>
    /// 学员的添加实体
    /// </summary>
    public class StudentAddViewModel
    {
        /// <summary>
        /// 姓名
        /// </summary>
        [DisplayName("姓名")]
        [Required(ErrorMessage = "请输入姓名")]
        [StringLength(4, ErrorMessage = "你的名字太长了，是火星来的么？")]
        public string Name { get; set; }

        /// <summary>
        /// 年龄
        /// </summary>
        [DisplayName("年龄")]
        [Required(ErrorMessage = "请输入年龄")]
        [Range(0, 130, ErrorMessage = "您寿比南山，打破了吉尼斯记录，你造吗？")]
        [RegularExpression(@"^\d{1,3}$", ErrorMessage = "您输入的格式不正确，请换个姿势再来一次")]
        public int Age { get; set; }

        /// <summary>
        /// 手机
        /// </summary>
        [DisplayName("手机")]
        [Required(ErrorMessage = "请输入手机")]
        [RegularExpression(@"^1[3578]\d{9}$", ErrorMessage = "您输入手机格式比较另类，无法与您取得联系~")]
        public string Mobile { get; set; }

        /// <summary>
        /// 邮箱
        /// </summary>
        [DisplayName("邮箱")]
        [Required(ErrorMessage = "请输入邮箱")]
        [DataType(DataType.EmailAddress, ErrorMessage = "您输入的邮箱格式不正确~")]
        public string Email { get; set; }

        /// <summary>
        /// 班级Id
        /// </summary>
        [DisplayName("班级")]
        [Required(ErrorMessage = "请选择班级")]
        public int ClassId { get; set; }
    }
}