﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HPIT.MVC.Models
{
    /// <summary>
    /// 学员的编辑实体
    /// </summary>
    public class StudentEditViewModel : StudentAddViewModel
    {
        /// <summary>
        /// 主键ID
        /// </summary>
        public int Id { get; set; }

    }
}