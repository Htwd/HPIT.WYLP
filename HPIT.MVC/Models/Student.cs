namespace HPIT.MVC.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Student")]
    public partial class Student
    {
        public int Id { get; set; }

        [Required]
        [StringLength(16)]
        public string Name { get; set; }

        public int Age { get; set; }

        [Required]
        [StringLength(16)]
        public string Mobile { get; set; }

        [Required]
        [StringLength(64)]
        public string Email { get; set; }

        public int? ClassId { get; set; }

        public DateTime AddTime { get; set; }

        public virtual Class Class { get; set; }
    }
}
