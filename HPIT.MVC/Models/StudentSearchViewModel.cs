﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HPIT.MVC.Models
{
    public class StudentSearchViewModel
    {
        public string Name { get; set; }
        public string Mobile { get; set; }
        public int ClassId { get; set; }
        public int page { get; set; }
        public int rows { get; set; }
    }
}