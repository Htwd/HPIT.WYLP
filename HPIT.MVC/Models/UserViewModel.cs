﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HPIT.MVC.Models
{
    /// <summary>
    /// 用户实体，用于视图显示数据
    /// </summary>
    public class UserViewModel
    {
        /// <summary>
        /// 姓名
        /// </summary>
        [Required(ErrorMessage ="请输入姓名")]
        [StringLength(4,ErrorMessage ="你的名字太长了，是火星来的么？")]
        public string Name { get; set; }

        /// <summary>
        /// 年龄
        /// </summary>
        [Range(0,130,ErrorMessage ="您寿比南山，打破了吉尼斯记录，你造吗？")]
        [RegularExpression(@"^\d{1,3}$",ErrorMessage ="您输入的格式不正确，请换个姿势再来一次")]
        public int Age { get; set; }

        /// <summary>
        /// 住址
        /// </summary>
        [StringLength(10,ErrorMessage ="您的地址太长了，我脑子不好使，记不住~")]
        public string Address { get; set; }
    }
}