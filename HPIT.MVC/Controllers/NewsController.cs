﻿using HPIT.MVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HPIT.MVC.Controllers
{
    public class NewsController : Controller
    {
        // GET: News
        public ActionResult Index(string year,string month,string day,int id)
        {
            //year = Request.QueryString["year"];
            //month = Request.QueryString["month"];
            //day = Request.QueryString["day"];
            //id = Convert.ToInt32(Request.QueryString["id"]);

            //直接通过Action定义的参数来获取http请求的数据，更方便快捷
            string str = $"{year}-{month}-{day}-{id}";

            //return Redirect("/Home/Index");//跳转URL
            //return RedirectToAction("Demo");//跳转Action
            //return RedirectToRoute("Default", new { controller = "Home", action = "Index"});//跳转路由

            var obj = new { Year = year, Month = month, Day = day, Id = id };

            //return Json(obj,JsonRequestBehavior.AllowGet); //返回json格式数据
            return View();
        }

        public ActionResult Demo()
        {
            ViewData["Data1"] = "我是ViewData数据";
            ViewBag.Data2 = "我是ViewBag数据";

            ViewBag.MyData = "物联5班到此一游~~";

            var model = new UserViewModel()
            {
                Address = "河南郑州",
                Age = 18,
                Name = "何老师"
            };
            ViewBag.UserInfo = model;

            return View();
        }

        public ActionResult Demo2()
        {
            var model = new UserViewModel()
            {
                Address = "河南郑州",
                Age = 18,
                Name = "何老师"
            };

            return View(model);
        }

        public ActionResult Demo3()
        {
            var model = new UserViewModel()
            {
                Address = "河南郑州",
                Age = 18,
                Name = "何老师"
            };

            return View(model);
        }

        //默认Get请求
        public ActionResult Register()
        {
            return View();
        }


        //指定Post请求
        [HttpPost]
        public ActionResult Register(UserViewModel model)
        {
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", "眼睛睁大一点，再填一遍~");
                return View(model);
            }
            //把数据写入到数据库中，返回消息提示，代码略。
            return View();
        }
        
    }
}