﻿using HPIT.MVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HPIT.MVC.Controllers
{
    public class HomeController : Controller
    {

        public ActionResult About()
        {
            ViewBag.Message = "Your application description pag111111e.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }


        #region 首页学员信息列表


        /// <summary>
        /// 首页学员信息列表
        /// </summary>
        /// <returns></returns>
        public ActionResult Default()
        {
            //1.实例化数据访问上文
            using (var db = new StudentModel())
            {
                //2.查询出所有的学员信息
                //3.构造对应的学员信息实体
                var model = db.Student.Where(s => true).Select(s => new StudentViewModel()
                {
                    Age = s.Age,
                    ClassId = (int)s.ClassId,
                    ClassName = s.Class.ClassName,
                    Email = s.Email,
                    Id = s.Id,
                    Mobile = s.Mobile,
                    Name = s.Name,
                }).ToList();
                return View(model);
            }



        }

        #endregion


        #region 添加学员



        /// <summary>
        /// 添加学员
        /// </summary>
        /// <returns></returns>
        public ActionResult Add()
        {
            var viewModel = new StudentEditViewModel();
            ViewBag.ClassInfo = GetClassInfo();
            return View(viewModel);
        }

        /// <summary>
        /// 提交添加新学员
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Add(StudentEditViewModel model)
        {
            //1.模型验证
            if (ModelState.IsValid)
            {
                //2.如果验证通过，写入新数据
                //2.1实例化数据访问上下文
                using (var db = new StudentModel())
                {
                    var entity = new Student()
                    {
                        AddTime = DateTime.Now,
                        Age = model.Age,
                        ClassId = model.ClassId,
                        Email = model.Email,
                        Mobile = model.Mobile,
                        Name = model.Name
                    };

                    //2.2调用Add方法
                    db.Student.Add(entity);
                    //2.3提交操作
                    db.SaveChanges();
                    ViewBag.Msg = "新学员添加成功！";
                }
            }
            else
            {   //3.如果验证不通过，消息提示
                ModelState.AddModelError("", "您输入的信息有误！");
            }
            ViewBag.ClassInfo = GetClassInfo();
            return View(model);
        }

        #endregion


        #region 修改学员

     

        /// <summary>
        /// 修改学员
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Edit(int id)
        {
            //1.实例化数据访问上下文
            using (var db = new StudentModel())
            {
                var viewModel = new StudentEditViewModel();
                //2.根据id查询对应的学员信息
                var student = db.Student.FirstOrDefault(s => s.Id == id);
                if (student != null)
                {
                    //3.构造视图需要的实体
                    viewModel = new StudentEditViewModel()
                    {
                        Age = student.Age,
                        ClassId = (int)student.ClassId,
                        Email = student.Email,
                        Id = student.Id,
                        Mobile = student.Mobile,
                        Name = student.Name
                    };
                }

                ViewBag.ClassInfo = GetClassInfo();
                return View(viewModel);
            }
        }

        /// <summary>
        /// 提交学员修改
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Edit(StudentEditViewModel viewModel)
        {
            //模型验证
            if (ModelState.IsValid)
            {
                //1.实例化数据访问上下文
                using (var db = new StudentModel())
                {
                    //2.查询出当前修改的学员
                    var model = db.Student.FirstOrDefault(s => s.Id == viewModel.Id);
                    //3.对学员的属性赋值
                    if (model != null)
                    {
                        model.Age = viewModel.Age;
                        model.ClassId = viewModel.ClassId;
                        model.Email = viewModel.Email;
                        model.Mobile = viewModel.Mobile;
                        model.Name = viewModel.Name;
                        //4.提交操作
                        db.SaveChanges();
                        ViewBag.Msg = "学员信息修改成功！";
                    }

                }
            }
            else
            {
                ModelState.AddModelError("", "您输入的信息有误！");
            }
            ViewBag.ClassInfo = GetClassInfo();
            return View(viewModel);

        }

        #endregion


        #region 删除学员

     

        /// <summary>
        /// 删除学员
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Delete(int id)
        {
            //1.实例化数据访问上下文
            using (var db = new StudentModel())
            {
                var result = new ResultModel();
                //2.查询当前删除的学员是否存在
                var model = db.Student.FirstOrDefault(s => s.Id == id);
                if (model != null)
                {
                    //3.如果存在则删除
                    //3.1调用Remove方法标记为删除
                    db.Student.Remove(model);
                    //3.2提交操作
                    if (db.SaveChanges() > 0)
                    {
                        result.IsSuccess = true;
                        result.ErrorMessage = "删除成功";
                    }
                    else
                    {
                        result.IsSuccess = false;
                        result.ErrorMessage = "网络繁忙，请稍后重试";
                    }
                }
                else
                {
                    //4.如果不存在则提示错误消息
                    result.IsSuccess = false;
                    result.ErrorMessage = "当前的学员已经删除！";

                }
                return Json(result);
            }

        }

        #endregion

        /// <summary>
        /// 获取学员的班级信息
        /// </summary>
        /// <returns></returns>
        private List<SelectListItem> GetClassInfo()
        {
            //1.实例化数据访问上下文
            using (var db = new StudentModel())
            {
                //2.查询学员班级信息
                //3.构造学员班级信息数据
                var classInfo = db.Class.Where(c => true).Select(c => new SelectListItem
                {
                    Text = c.ClassName,
                    Value = c.Id.ToString()
                }).ToList();
                return classInfo;

            }
        }

        #region EasyUI版相关方法


        /// <summary>
        /// 加载学员列表页
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 获取学员列表信息
        /// </summary>
        /// <returns></returns>
        public ActionResult GetStudentList(StudentSearchViewModel viewModel)
        {
            //1.实例化数据访问上文
            using (var db = new StudentModel())
            {
                //2.查询出所有的学员信息
                //3.构造对应的学员信息实体
                var query = db.Student.AsQueryable();
                if (!string.IsNullOrWhiteSpace(viewModel.Name))
                {
                    query = query.Where(e => e.Name.Contains(viewModel.Name));
                }
                if (!string.IsNullOrWhiteSpace(viewModel.Mobile))
                {
                    query = query.Where(e => e.Mobile == viewModel.Mobile);
                }
                if (viewModel.ClassId > 0)
                {
                    query = query.Where(e => e.ClassId == viewModel.ClassId);
                }

                var model = query.Select(s => new StudentViewModel()
                {
                    Age = s.Age,
                    ClassId = (int)s.ClassId,
                    ClassName = s.Class.ClassName,
                    Email = s.Email,
                    Id = s.Id,
                    Mobile = s.Mobile,
                    Name = s.Name,
                    AddTime = s.AddTime
                }).OrderBy(s=>s.Id).Skip((viewModel.page - 1) * viewModel.rows).Take(viewModel.rows).ToList();

                var totalCount = query.Count();
                var result = new { total = totalCount, rows = model };
                //4.返回Json格式的数据
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// 获取班级信息
        /// </summary>
        /// <returns></returns>
        public ActionResult GetClassData()
        {
            //1.实例化数据访问上下文
            using (var db = new StudentModel())
            {
                //2.读取班级信息构造班级下拉框所需的实体
                var classList = db.Class.Select(c => new { Id = c.Id, ClassName = c.ClassName }).ToList();
                //3.返回Json格式的数据
                return Json(classList, JsonRequestBehavior.AllowGet);
            }

        }

        /// <summary>
        /// 添加新学员
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AddStudent(StudentAddViewModel model)
        {
            var result = new ResultModel();
            //1.模型验证
            if (ModelState.IsValid)
            {
                //2.如果验证通过，写入新数据
                //2.1实例化数据访问上下文
                using (var db = new StudentModel())
                {
                    var entity = new Student()
                    {
                        AddTime = DateTime.Now,
                        Age = model.Age,
                        ClassId = model.ClassId,
                        Email = model.Email,
                        Mobile = model.Mobile,
                        Name = model.Name
                    };

                    //2.2调用Add方法
                    db.Student.Add(entity);
                    //2.3提交操作
                    db.SaveChanges();
                    
                    result.IsSuccess = true;
                    result.ErrorMessage = "学员添加成功！";
                }
            }
            else
            {   //3.如果验证不通过，消息提示
                result.IsSuccess = false;
                result.ErrorMessage = "您输入的内容有误！";
            }
           
            //4.返回操作结果的json格式数据
            return Json(result);
        }

        /// <summary>
        /// 编辑学员信息
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult EditStudent(StudentEditViewModel viewModel)
        {
            var result = new ResultModel();
            //模型验证
            if (ModelState.IsValid)
            {
                //1.实例化数据访问上下文
                using (var db = new StudentModel())
                {
                    //2.查询出当前修改的学员
                    var model = db.Student.FirstOrDefault(s => s.Id == viewModel.Id);
                    //3.对学员的属性赋值
                    if (model != null)
                    {
                        model.Age = viewModel.Age;
                        model.ClassId = viewModel.ClassId;
                        model.Email = viewModel.Email;
                        model.Mobile = viewModel.Mobile;
                        model.Name = viewModel.Name;
                        //4.提交操作
                        db.SaveChanges();

                        result.IsSuccess = true;
                        result.ErrorMessage = "学员信息修改成功！";
                    }
                }
            }
            else
            {
                //验证不通过，则提示错误消息
                result.IsSuccess = false;
                result.ErrorMessage = "您输入的内容有误！";
            }
          
            return Json(result);
        }

        #endregion



    }
}