﻿using HPIT.Web.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HPIT.Web
{
    public partial class Add : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //1.获取到学员班级信息
            if (!IsPostBack)
            {
                BindClass();
            }
        }

        /// <summary>
        /// 获取学员的班级信息并绑定数据
        /// </summary>
        private void BindClass()
        {

            #region 使用ADO.NET实现



            ////1.定义查询SQL语句
            //string sql = "SELECT * FROM dbo.Class";
            ////2.通过DbHelper来查询数据
            //DataTable dt = DbHelper.ExecuteTable(sql);
            ////3.进行下拉框的数据绑定
            //ddl_Class.DataSource = dt;
            //ddl_Class.DataTextField = "ClassName";
            //ddl_Class.DataValueField = "Id";
            //ddl_Class.DataBind();

            #endregion

            #region 使用EF实现

            using (var db = new DemoEntities())
            {
                //1.查询班级数据
                var list = (from c in db.Class select c).ToList();
                //2.将数据绑定到下拉框
                ddl_Class.DataSource = list;
                ddl_Class.DataTextField = "ClassName";
                ddl_Class.DataValueField = "Id";
                ddl_Class.DataBind();
            }

            #endregion


        }

        protected void btn_Add_Click(object sender, EventArgs e)
        {
            //1.判断数据验证是否通过
            if (IsValid)
            {
                #region 使用ADO.NET

                ////2.写入数据库新的信息
                ////2.1定义插入SQL语句
                //string sql = "INSERT INTO dbo.Student( Name , Age , Mobile ,  Email , ClassId , AddTime) VALUES  ( @Name , @Age ,@Mobile , @Email ,@ClassId , @AddTime)";
                ////2.2定义参数
                //SqlParameter[] pams = {
                //    new SqlParameter("@Name",txt_Name.Text.Trim()),
                //    new SqlParameter("@Age", Convert.ToInt32(txt_Age.Text)),
                //    new SqlParameter("@Mobile",txt_Mobile.Text.Trim()),
                //    new SqlParameter("@Email",txt_Email.Text.Trim()),
                //    new SqlParameter("@ClassId ",Convert.ToInt32( ddl_Class.SelectedValue)),
                //    new SqlParameter("@AddTime",DateTime.Now),
                //};
                ////2.3通过DbHelper执行操作
                ////2.4提示用户信息
                //if (DbHelper.ExecuteNonQuery(sql, pams) > 0)
                //{
                //    lbl_Msg.Text = "新学员添加成功";
                //}
                //else
                //{
                //    lbl_Msg.Text = "新学员添加失败，人品不行~~";
                //}

                #endregion

                #region 使用EF

                using (var db = new DemoEntities())
                {
                    //1.实例化一个学员对象
                    var model = new Student()
                    {
                        AddTime=DateTime.Now,
                        Age = Convert.ToInt32(txt_Age.Text),
                        ClassId= Convert.ToInt32(ddl_Class.SelectedValue),
                        Email= txt_Email.Text.Trim(),
                        Mobile= txt_Mobile.Text.Trim(),
                        Name= txt_Name.Text.Trim()
                    };

                    //2.放入EF容器
                    //2.1先把新的实体对象放入EF容器中
                    //2.2为实体打上新增的标签
                    //db.Student.Add(model);

                    //告诉EF当前实体是新增状态
                    db.Entry<Student>(model).State = System.Data.Entity.EntityState.Added;


                    //3.执行操作
                    if (db.SaveChanges() > 0)
                    {
                        lbl_Msg.Text = "新学员添加成功";
                    }
                    else
                    {
                        lbl_Msg.Text = "新学员添加失败，人品不行~~";
                    }


                }

                #endregion

            }

        }
    }
}