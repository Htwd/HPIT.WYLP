﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="Add.aspx.cs" Inherits="HPIT.Web.Add" %>
<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <h2>添加新学员</h2>
    <p class="text-danger">
        <asp:Literal runat="server" ID="ErrorMessage" />
    </p>

    <div class="form-horizontal">
        <hr />
        <asp:ValidationSummary runat="server" CssClass="text-danger" />
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="txt_Name" CssClass="col-md-2 control-label">姓名</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="txt_Name" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="txt_Name"
                    CssClass="text-danger" ErrorMessage="请输入姓名" />
            </div>
        </div>
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="txt_Age" CssClass="col-md-2 control-label">年龄</asp:Label>
            <div class="col-md-3">
                <asp:TextBox runat="server" ID="txt_Age" TextMode="Number" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="txt_Age"
                    CssClass="text-danger" ErrorMessage="请输入年龄" />
            </div>
        </div>
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="txt_Mobile" CssClass="col-md-2 control-label">手机</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="txt_Mobile" TextMode="Phone" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="txt_Mobile"
                    CssClass="text-danger" Display="Dynamic" ErrorMessage="请输入手机号码" />
                
            </div>
        </div>
         <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="txt_Email" CssClass="col-md-2 control-label">邮箱</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="txt_Email" CssClass="form-control" TextMode="Email" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="txt_Email"
                    CssClass="text-danger" ErrorMessage="请输入邮箱" />
            </div>
        </div>
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="txt_Email" CssClass="col-md-2 control-label">班级</asp:Label>
            <div class="col-md-3">
               <asp:DropDownList ID="ddl_Class" runat="server"  CssClass="form-control"></asp:DropDownList>
            </div>
           
        </div>
        <div class="form-group">
            <div class="col-md-offset-2 col-md-10">
                <asp:Button runat="server" ID="btn_Add" Text="添加学员" CssClass="btn btn-success" OnClick="btn_Add_Click" />
            </div>
        </div>
        <div class="form-group">
             <div class="col-md-offset-2 col-md-10">
            <asp:Label ID="lbl_Msg" runat="server" ForeColor="Red"></asp:Label>
        </div>
            </div>
    </div>
</asp:Content>