﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="HPIT.Web._Default" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <h2>学生列表</h2>

    <div class="table-responsive">
        <table class="table table-striped ">
            <thead>
                <tr>
                    <th>编号</th>
                    <th>姓名</th>
                    <th>年龄</th>
                    <th>手机</th>
                    <th>邮箱</th>
                    <th>班级</th>
                    <th>操作</th>
                </tr>
            </thead>
            <tbody>
                <asp:Repeater ID="rpt_Student" runat="server">
                    <ItemTemplate>
                        <tr>
                            <td><%# Eval("Id") %></td>
                            <td><%# Eval("Name") %></td>
                            <td><%# Eval("Age") %></td>
                            <td><%# Eval("Mobile") %></td>
                            <td><%# Eval("Email") %></td>
                            <td><%# Eval("ClassName") %></td>
                            <td>
                                <div class="btn-group">
                                    <a href="Edit.aspx?id=<%# Eval("Id") %>" class="btn btn-warning" style="margin-right: 10px;">修改</a><a href="javascript:;" onclick="deleteStudent(<%# Eval("Id") %>)" class="btn btn-danger">删除</a>
                                </div>

                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </tbody>
        </table>
        <a href="Add.aspx" class="btn btn-success">添加</a>

    </div>
    <script>
        //删除学员
        function deleteStudent(id)
        {
            //如果确认删除则进行删除操作
            if (confirm("确定要删除吗？"))
            {
                //使用Ajax请求删除该记录
                $.getJSON("/ashx/StudentHandler.ashx", { Id: id }, function (data) {
                    //如果执行成功则刷新页面重新进行数据绑定
                    if (data.IsSuccess) {
                        location.reload();
                    }
                    else {
                        alert(data.ErrorMessage);
                    }
                });
            }
        }

    </script>
</asp:Content>
