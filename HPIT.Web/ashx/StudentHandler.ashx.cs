﻿using HPIT.Web.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace HPIT.Web.ashx
{
    /// <summary>
    /// StudentHandler 的摘要说明
    /// </summary>
    public class StudentHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            //1.接受传递过来的Id
            int id = Convert.ToInt32(context.Request.QueryString["id"]);

            ResultModel result = new ResultModel();

            #region 使用ADO.NET实现




            ////2.判断该学员是否存在
            ////2.1定义SQL语句
            //string sql = "SELECT COUNT(*) FROM dbo.Student WHERE Id=@Id";
            ////2.2定义参数
            //SqlParameter[] pams = {
            //    new SqlParameter("@Id",id)
            //};
            ////2.3通过DbHelper执行查询
            //if (Convert.ToInt32(DbHelper.ExecuteScalar(sql, pams)) > 0)
            //{
            //    //3.如果存在则删除，否则提示用户该学员已删除
            //    //3.1定义删除SQL语句
            //    string sqlDelete = "DELETE FROM dbo.Student WHERE Id=@Id";
            //    //3.2定义参数
            //    SqlParameter[] pamsDelete = {
            //    new SqlParameter("@Id",id)
            //};
            //    //3.3通过DbHelper执行删除操作
            //    if (DbHelper.ExecuteNonQuery(sqlDelete, pamsDelete) > 0)
            //    {
            //        result.IsSuccess = true;
            //        result.ErrorMessage = "删除成功";
            //    }
            //    else
            //    {
            //        result.IsSuccess = false;
            //        result.ErrorMessage = "网络异常，请稍候重试";
            //    }


            //}
            //else
            //{
            //    result.IsSuccess = false;
            //    result.ErrorMessage = "该学员已删除！";
            //}

            #endregion

            #region 使用EF实现

            using (var db = new DemoEntities())
            {
                //1.先查询出当前学员

                var model = db.Student.FirstOrDefault(s => s.Id == id);
                if (model != null)
                {
                    //2.删除学员
                    //db.Student.Remove(model);

                    //为当前实体标记为删除状态
                    db.Entry<Student>(model).State = System.Data.Entity.EntityState.Deleted;

                    //3.执行操作
                    if (db.SaveChanges() > 0)
                    {
                        result.IsSuccess = true;
                        result.ErrorMessage = "删除成功";
                    }
                    else
                    {
                        result.IsSuccess = false;
                        result.ErrorMessage = "网络异常，请稍候重试";
                    }
                }
                else
                {
                    result.IsSuccess = false;
                    result.ErrorMessage = "该学员已删除！";
                }


                
            }


           #endregion



                JavaScriptSerializer js = new JavaScriptSerializer();
            context.Response.Write(js.Serialize(result));

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}