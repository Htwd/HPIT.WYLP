﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(HPIT.Web.Startup))]
namespace HPIT.Web
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
