﻿using HPIT.Web.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HPIT.Web
{
    public partial class Edit : System.Web.UI.Page
    {
        //当前学员的ID
        private int editId = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //1.获取到传递过来的ID
                if (!string.IsNullOrWhiteSpace(Request.QueryString["Id"]))
                {
                    editId = Convert.ToInt32(Request.QueryString["Id"]);
                }
                //绑定学员信息
                BindStudentInfo();
            }


        }

        /// <summary>
        /// 绑定学员信息
        /// </summary>
        private void BindStudentInfo()
        {
            //2.根据Id获取到当前的学员信息
            if (editId > 0)
            {
                #region 使用ADO.NET来实现


                ////2.1定义查询SQL
                //string sql = "SELECT * FROM dbo.Student WHERE Id=@Id";
                ////2.2定义参数化查询的参数
                //SqlParameter[] pams = {
                //    new SqlParameter("@Id",editId)
                //};
                ////2.3通过DbHelper来执行查询语句获取学员的信息
                //using (SqlDataReader reader = DbHelper.ExecuteReader(sql, pams))
                //{
                //    //先判断reader里面有没有行数据
                //    if (reader.HasRows)
                //    {
                //        //向下移动一次游标
                //        if (reader.Read())
                //        {
                //            //开始读取数据 放在对象中
                //            StudentModel model = new StudentModel()
                //            {
                //                Age = Convert.ToInt32(reader["Age"]),
                //                ClassId = Convert.ToInt32(reader["ClassId"]),
                //                Email = reader["Email"].ToString(),
                //                Id = Convert.ToInt32(reader["Id"]),
                //                Mobile = reader["Mobile"].ToString(),
                //                Name = reader["Name"].ToString()
                //            };
                //            //2.4将学员的信息绑定到页面上
                //            txt_Name.Text = model.Name;
                //            txt_Age.Text = model.Age.ToString();
                //            txt_Email.Text = model.Email;
                //            txt_Mobile.Text = model.Mobile;

                //            //3.绑定班级的下拉框
                //            //3.1读取出班级信息
                //            //1.定义查询SQL语句
                //            string sqlCalss = "SELECT * FROM dbo.Class";
                //            //2.通过DbHelper来查询数据
                //            DataTable dt = DbHelper.ExecuteTable(sqlCalss);
                //            //3.进行下拉框的数据绑定
                //            ddl_Class.DataSource = dt;
                //            ddl_Class.DataTextField = "ClassName";
                //            ddl_Class.DataValueField = "Id";
                //            ddl_Class.DataBind();
                //            //3.3使下拉框的选中项指向当前学员的班级ID
                //            ddl_Class.SelectedValue = model.ClassId.ToString();

                //        }
                //    }
                //}

                #endregion

                #region 使用EF来实现

                using (var db = new DemoEntities())
                {
                    //1.查询出当前修改的学员信息
                    var model = db.Student.FirstOrDefault(s => s.Id == editId);
                    if (model != null)
                    {
                        //2.绑定学员信息到界面上
                        txt_Name.Text = model.Name;
                        txt_Age.Text = model.Age.ToString();
                        txt_Email.Text = model.Email;
                        txt_Mobile.Text = model.Mobile;

                        //3.查询出班级信息

                        //3.1.查询班级数据
                        var list = (from c in db.Class select c).ToList();
                        //3.2.将数据绑定到下拉框
                        ddl_Class.DataSource = list;
                        ddl_Class.DataTextField = "ClassName";
                        ddl_Class.DataValueField = "Id";
                        ddl_Class.DataBind();

                        //4.使班级下拉框选中当前学员的班级
                        ddl_Class.SelectedValue = model.ClassId.ToString();

                    }




                }


                #endregion
            }



        }

        protected void btn_Update_Click(object sender, EventArgs e)
        {
            //1.判断数据验证是否通过
            if (IsValid)
            {
                //获取到传递过来的ID
                if (!string.IsNullOrWhiteSpace(Request.QueryString["Id"]))
                {
                    editId = Convert.ToInt32(Request.QueryString["Id"]);
                }


                #region 使用ADO.NET实现



                ////2.写入数据库新的信息
                ////2.1定义更新SQL语句
                //string sql = "UPDATE dbo.Student SET Name=@Name , Age=@Age , Mobile=@Mobile ,  Email=@Email , ClassId=@ClassId WHERE Id=@Id";
                ////2.2定义参数
                //SqlParameter[] pams = {
                //    new SqlParameter("@Name",txt_Name.Text.Trim()),
                //    new SqlParameter("@Age", Convert.ToInt32(txt_Age.Text)),
                //    new SqlParameter("@Mobile",txt_Mobile.Text.Trim()),
                //    new SqlParameter("@Email",txt_Email.Text.Trim()),
                //    new SqlParameter("@ClassId ",Convert.ToInt32( ddl_Class.SelectedValue)),
                //    new SqlParameter("@Id ",editId)

                //};
                ////2.3通过DbHelper执行操作
                ////2.4提示用户信息
                //if (DbHelper.ExecuteNonQuery(sql, pams) > 0)
                //{
                //    lbl_Msg.Text = "学员更新成功";
                //}
                //else
                //{
                //    lbl_Msg.Text = "学员更新失败，人品不行~~";
                //}


                #endregion

                #region 使用EF实现

                using (var db = new DemoEntities())
                {
                    //1.查询出当前的学员信息
                    var model = db.Student.FirstOrDefault(s => s.Id == editId);
                    if (model != null)
                    {
                        //2.更新现有属性
                        model.Age = Convert.ToInt32(txt_Age.Text);
                        model.ClassId = Convert.ToInt32(ddl_Class.SelectedValue);
                        model.Email = txt_Email.Text.Trim();
                        model.Mobile = txt_Mobile.Text.Trim();
                        model.Name = txt_Name.Text.Trim();

                        //3.执行操作
                        if (db.SaveChanges() > 0)
                        {
                            lbl_Msg.Text = "学员更新成功";
                        }
                        else
                        {
                            lbl_Msg.Text = "学员更新失败，人品不行~~";
                        }

                    }
                }

                #endregion

            }
        }
    }
}