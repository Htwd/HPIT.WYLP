﻿using HPIT.Web.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HPIT.Web
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //1.读取数据库中所有学员的信息
            if (!IsPostBack)
            {
                BindStudentList();
            }
        }

        /// <summary>
        /// 获取所有学员信息并进行绑定
        /// </summary>
        private void BindStudentList()
        {

            #region 使用ADO.NET实现


            ////1.定义查询SQL语句
            //string sql = "SELECT s.*,c.ClassName FROM dbo.Student AS s LEFT JOIN dbo.Class AS c ON s.ClassId=c.Id";
            ////2.通过DbHelper进行数据查询
            //using (SqlDataReader reader = DbHelper.ExecuteReader(sql))
            //{
            //    //2.1先判断reader里面有没有行数据
            //    if (reader.HasRows)
            //    {
            //        //2.2移动游标获取每一行数据
            //        List<StudentModel> list = new List<StudentModel>();
            //        while (reader.Read())
            //        {
            //            //2.3开始读取数据 放在对象中
            //            StudentModel model = new StudentModel()
            //            {
            //                Age = Convert.ToInt32(reader["Age"]),
            //                ClassId = Convert.ToInt32(reader["ClassId"]),
            //                ClassName = reader["ClassName"].ToString(),
            //                Email = reader["Email"].ToString(),
            //                Id = Convert.ToInt32(reader["Id"]),
            //                Mobile = reader["Mobile"].ToString(),
            //                Name = reader["Name"].ToString()
            //            };
            //            list.Add(model);
            //        }
            //        //3.进行数据绑定
            //        rpt_Student.DataSource = list;
            //        rpt_Student.DataBind();

            //    }
            //}

            #endregion


            #region 使用EF实现
     
            using (var db = new DemoEntities())
            {
                //1.查询学员数据
                var list = db.Student.Where(s => true).ToList();

             

                List<StudentModel> listModels = new List<StudentModel>();

                list.ForEach(s =>
                {
                    var model = new StudentModel()
                    {
                        Age = s.Age,
                        ClassId = (int)s.ClassId,
                        ClassName = s.Class.ClassName,
                        Email = s.Email,
                        Id = s.Id,
                        Mobile = s.Mobile,
                        Name = s.Name

                    };

                    listModels.Add(model);

                });

           
                //2.完成数据绑定
                rpt_Student.DataSource = listModels;
                rpt_Student.DataBind();

            }



            #endregion

        }
    }
}