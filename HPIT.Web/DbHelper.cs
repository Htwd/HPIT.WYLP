﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace HPIT.Web
{
    /// <summary>
    /// 数据库访问助手类
    /// </summary>
    public class DbHelper
    {

        //1.定义数据库连接字符串
        private static readonly string connStr = ConfigurationManager.ConnectionStrings["connStr"].ConnectionString;

        /// <summary>
        /// 执行增删改操作
        /// </summary>
        /// <param name="sql">SQL语句</param>
        /// <param name="parameters">参数</param>
        /// <returns>返回受影响行数</returns>
        public static int ExecuteNonQuery(string sql, params SqlParameter[] parameters)
        {
            //使用using执行完之后释放资源
            using (SqlConnection conn = new SqlConnection(connStr))
            {
                //创建SqlCommand对象
                using (SqlCommand cmd = new SqlCommand(sql, conn))
                {
                    //添加参数
                    if (parameters != null)
                    {
                        cmd.Parameters.AddRange(parameters);
                    }
                    //打开连接，先判断状态是否关闭
                    if (conn.State == ConnectionState.Closed)
                    {
                        conn.Open();
                    }
                    //执行操作
                    return cmd.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// 查询首行首列
        /// </summary>
        /// <param name="sql">SQL语句</param>
        /// <param name="parameters">参数</param>
        /// <returns>返回首行首列</returns>
        public static object ExecuteScalar(string sql, params SqlParameter[] parameters)
        {
            //使用using执行完之后释放资源
            using (SqlConnection conn = new SqlConnection(connStr))
            {
                //创建SqlCommand对象
                using (SqlCommand cmd = new SqlCommand(sql, conn))
                {
                    //添加参数
                    if (parameters != null)
                    {
                        cmd.Parameters.AddRange(parameters);
                    }
                    //打开连接，先判断状态是否关闭
                    if (conn.State == ConnectionState.Closed)
                    {
                        conn.Open();
                    }
                    //执行操作
                    return cmd.ExecuteScalar();
                }
            }
        }

        /// <summary>
        /// 查询列表
        /// </summary>
        /// <param name="sql">SQL语句</param>
        /// <param name="parameters">参数</param>
        /// <returns>返回SqlDataReader</returns>
        public static SqlDataReader ExecuteReader(string sql, params SqlParameter[] parameters)
        {
            //使用using执行完之后释放资源
            SqlConnection conn = new SqlConnection(connStr);
            //创建SqlCommand对象
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                //添加参数
                if (parameters != null)
                {
                    cmd.Parameters.AddRange(parameters);
                }
                //打开连接，先判断状态是否关闭
                if (conn.State == ConnectionState.Closed)
                {
                    conn.Open();
                }
                //执行操作
                return cmd.ExecuteReader(CommandBehavior.CloseConnection);
            }

        }

        /// <summary>
        /// 查询列表
        /// </summary>
        /// <param name="sql">SQL语句</param>
        /// <param name="parameters">参数</param>
        /// <returns>返回DataTable</returns>
        public static DataTable ExecuteTable(string sql, params SqlParameter[] parameters)
        {
            //使用using执行完之后释放资源
            using (SqlDataAdapter sda = new SqlDataAdapter(sql, connStr))
            {
                //添加参数
                if (parameters != null)
                {
                    sda.SelectCommand.Parameters.AddRange(parameters);
                }
                //将数据由暂存集中填充至DataTable中
                using (DataTable dt = new DataTable())
                {
                    sda.Fill(dt);
                    return dt;
                }
                
            }
            
        }
    }
}