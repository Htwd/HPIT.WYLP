﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HPIT.Web.Models
{
    /// <summary>
    /// 返回结果消息
    /// </summary>
    public class ResultModel
    {
        /// <summary>
        /// 是否操作成功
        /// </summary>
        public bool IsSuccess { get; set; }

        /// <summary>
        /// 错误消息提示
        /// </summary>
        public string ErrorMessage { get; set; }
    }
}